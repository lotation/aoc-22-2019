#ifndef AOC_MACRO_H
#define AOC_MACRO_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <assert.h>


#define ERROR_ON   "\e[31m"
#define BOLD_ON    "\e[1m"
#define COLOR_OFF  "\e[m"

#define BASE 10
#define OP_COUNT 3
#define MAX_LINE 128
#define MAX_OP_DIGITS 33

//#define SWAP(A,B,T) do { T tmp = A; A = B; B = tmp; } while(0);
//#define UNUSED(A) (void)(A)
//#define STRINGIFY(A) #A

#define ERROR(MSG) fprintf(stderr, BOLD_ON "%s:%d:" COLOR_OFF " %s: "  ERROR_ON "error:" COLOR_OFF " %s: %s\n", __FILE__, __LINE__, __func__, MSG, strerror(errno)); 
#define MCHECK(PTR) if (PTR == NULL) { ERROR("malloc"); exit(EXIT_FAILURE); }
#define FOPEN(FILENAME) (FILE*)({ errno = 0; FILE *FP = fopen(FILENAME, "r"); if (FP  == NULL) { ERROR(FILENAME); exit(EXIT_FAILURE); }; FP; })
#define MANY(TYPE,COUNT) (TYPE*)({ errno = 0; TYPE *RET = calloc(COUNT, sizeof(TYPE)); MCHECK(RET); RET; })
#define NEW(TYPE) MANY(TYPE,1)


#define GEN_AOC(TYPE,SYMBOL) \
    GEN_STRUCT(TYPE) \
    GEN_HEADER(TYPE) \
    GEN_SOURCE(TYPE,SYMBOL) \


#define GEN_STRUCT(TYPE) \
\
    struct _##TYPE##_array_s { \
        size_t length; \
        TYPE *p; \
    }; \
    \
    struct _##TYPE##_operation_s { \
        char *op_name; \
        TYPE *op_value; \
        void (*TYPE##_op) (struct _##TYPE##_array_s *array, TYPE *value); \
    }; \
    \
    struct _##TYPE##_oparray_s { \
        size_t length; \
        struct _##TYPE##_operation_s *p; \
    }; \
\


#define GEN_HEADER(TYPE) \
\
    typedef struct _##TYPE##_array_s      TYPE##_array_s; \
    typedef struct _##TYPE##_operation_s  TYPE##_operation_s; \
    typedef struct _##TYPE##_oparray_s    TYPE##_oparray_s; \
    /* typedef void (*operation)(TYPE##_array_s *array, TYPE *value); */ \
\
\
    size_t TYPE##_compute(TYPE##_oparray_s *operations, size_t cards_number, size_t times, TYPE to_match); \
    void   TYPE##_parse_iput(char *filename, TYPE##_oparray_s *operations); \
\
    void TYPE##_deal_into_new_stack(TYPE##_array_s *array, TYPE *value); \
    void TYPE##_deal_with_increment(TYPE##_array_s *array, TYPE *value); \
    void TYPE##_cut                (TYPE##_array_s *array, TYPE *value); \
\


#define GEN_SOURCE(TYPE,SYMBOL) \
\
    /* extern const TYPE##_operation_s TYPE##_ops_map[OP_COUNT]; */ \
    const TYPE##_operation_s TYPE##_ops_map[OP_COUNT] = { \
        { "deal into new stack", NULL, TYPE##_deal_into_new_stack }, \
        { "deal with increment", NULL, TYPE##_deal_with_increment }, \
        { "cut",                 NULL, TYPE##_cut                 }, \
    }; \
\
    size_t TYPE##_compute(TYPE##_oparray_s *operations, size_t cards_number, size_t times, TYPE to_match) \
    { \
        TYPE##_array_s cards; \
        cards.length = cards_number; \
        cards.p = MANY(TYPE, cards.length); \
        \
        for (size_t i = 0; i < cards.length; i++) { \
            cards.p[i] = i; \
        } \
        \
        for (size_t i = 0; i < times; i++) \
        { \
            for (size_t op_indx = 0; op_indx < operations->length; op_indx++) { \
                operations->p[op_indx].TYPE##_op(&cards, operations->p[op_indx].op_value); \
            } \
        } \
        \
        size_t result = -1; \
        \
        for (size_t i = 0; i < cards.length; i++) { \
            if (cards.p[i] == to_match) { \
                result = i; \
                goto cleanup; \
            } \
        } \
        \
    cleanup: \
        free(cards.p); \
        return result; \
    } \
\
    void TYPE##_parse_iput(char *filename, TYPE##_oparray_s *operations) \
    { \
        if (operations == NULL) { \
            return; \
        } \
        \
        FILE *fp = FOPEN(filename); \
        \
        int c = 0; \
        char name_buffer[MAX_LINE]; \
        char val_buffer[MAX_OP_DIGITS]; \
        size_t name_indx = 0, val_indx = 0, op_indx = 0; \
        TYPE val = 0; \
        \
        while ((c = getc(fp)) != EOF) { \
            if ((c >= 'a' && c <= 'z') || c == ' ') { \
                name_buffer[name_indx++] = c; \
            } else if ((c >= '0' && c <= '9') || c == '-') { \
                name_buffer[name_indx] = '\0'; \
                val_buffer[val_indx++] = c; \
            } else if (c == '\n') { \
                val_buffer[val_indx] = '\0'; \
                \
                val = strto##SYMBOL(val_buffer, NULL, BASE); \
                \
                for (size_t k = 0; k < OP_COUNT; k++) { \
                    assert(TYPE##_ops_map[k].op_name != NULL); \
                    assert(TYPE##_ops_map[k].TYPE##_op != NULL); \
                    \
                    if (strstr(name_buffer, TYPE##_ops_map[k].op_name) != NULL) { \
                        /* check for space */ \
                        if (op_indx + 1 > operations->length) { \
                            operations->length  += 1; \
                            TYPE##_operation_s *tmp = (TYPE##_operation_s *) reallocarray(operations->p, operations->length, sizeof(TYPE##_operation_s)); \
                            MCHECK(tmp); \
                            operations->p = tmp; \
                        } \
                        \
                        /* copy name */ \
                        operations->p[op_indx].op_name = MANY(char, strlen(name_buffer) + 1); \
                        memcpy(operations->p[op_indx].op_name, name_buffer, strlen(name_buffer) + 1); \
                        \
                        if (strstr(operations->p[op_indx].op_name, "stack") != NULL) { \
                            operations->p[op_indx].op_value = NULL; \
                        } else { \
                            /* copy value */ \
                            operations->p[op_indx].op_value = NEW(TYPE); \
                            memcpy(operations->p[op_indx].op_value, &val, sizeof(TYPE)); \
                        } \
                        \
                        /* set operation */ \
                        operations->p[op_indx].TYPE##_op = TYPE##_ops_map[k].TYPE##_op; \
                        \
                        op_indx += 1; \
                    } \
                } \
                \
                val_indx = name_indx = 0; \
            } \
        } \
        \
        fclose(fp); \
    } \
\
    void TYPE##_deal_into_new_stack(TYPE##_array_s *array, TYPE *value) \
    { \
        if (value != NULL) { \
            fprintf(stderr, "value should be set to NULL in 'deal into new stack'!\n"); \
            exit(EXIT_FAILURE); \
        } \
        \
        for (size_t i = 0; i < ceil((double)array->length / 2); i++) { \
            TYPE tmp = array->p[i]; \
            array->p[i] = array->p[array->length - i - 1]; \
            array->p[array->length - i - 1] = tmp; \
        } \
    } \
\
    void TYPE##_deal_with_increment(TYPE##_array_s *array, TYPE *value) \
    { \
        if (value == NULL) { \
            fprintf(stderr, "value shouldn't be NULL in 'deal with increment'!\n"); \
            exit(EXIT_FAILURE); \
        } \
        \
        if (*value == 0) { \
            return; \
        } \
        \
        size_t arr_indx = 0, new_indx = 0; \
        TYPE##_array_s *new = NEW(TYPE##_array_s); \
        new->length = array->length; \
        new->p = MANY(TYPE, new->length); \
        \
        while (new_indx < array->length) { \
            new->p[arr_indx % array->length] = array->p[new_indx]; \
            arr_indx += *value; \
            new_indx++; \
        } \
        \
        TYPE##_array_s tmp = *array; \
        *array = *new; \
        *new = tmp; \
        \
        free(new->p); \
        free(new); \
    } \
\
    void TYPE##_cut(TYPE##_array_s *array, TYPE *value) \
    { \
        if (value == NULL) { \
            fprintf(stderr, "value shouldn't be NULL in 'cut'!\n"); \
            exit(EXIT_FAILURE); \
        } \
        \
        if (*value < 0) { \
            *value = array->length + *value; \
        } \
        \
        TYPE *tmp = MANY(TYPE, *value); \
        \
        for (size_t i = 0; i < (size_t)(*value); i++) { \
            tmp[i] = array->p[i]; \
        } \
        \
        for (size_t i = 0; i < array->length - *value; i++) { \
            array->p[i] = array->p[i + *value]; \
        } \
        \
        for (size_t i = 0; i < (size_t)(*value); i++) { \
            array->p[array->length - i - 1] = tmp[*value - i - 1]; \
        } \
        \
        free(tmp); \
    } \
\


#define CALC_AOC(TYPE, FORMAT_SPEC, CARDS, TIMES, TO_MATCH) \
\
    do { \
        TYPE##_oparray_s *TYPE##_operazioni = NEW(TYPE##_oparray_s); \
        TYPE##_operazioni->length = 1; \
        TYPE##_operazioni->p = MANY(TYPE##_operation_s, TYPE##_operazioni->length); \
        \
        TYPE##_parse_iput(argv[1], TYPE##_operazioni); \
        \
        size_t TYPE##_res = TYPE##_compute(TYPE##_operazioni, CARDS, TIMES, TO_MATCH); \
        printf("cards[%zu] => %" #FORMAT_SPEC "\n", TYPE##_res, (TYPE)TO_MATCH); \
        \
        /* cleanup */ \
        for (size_t i = 0; i < TYPE##_operazioni->length; i++) { \
            free(TYPE##_operazioni->p[i].op_name); \
            free(TYPE##_operazioni->p[i].op_value); \
        } \
        free(TYPE##_operazioni->p); \
        free(TYPE##_operazioni); \
    } while (0); \
\


#endif