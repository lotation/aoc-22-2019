#ifndef AOC_H
#define AOC_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>


//#define ERROR_ON   "\e[31m"
//#define BOLD_ON    "\e[1m"
//#define COLOR_OFF  "\e[m"

#define BASE 10
#define OP_COUNT 3
#define MAX_LINE 128
#define MAX_OP_DIGITS 33

//#define SWAP(A,B,T) do { T tmp = A; A = B; B = tmp; } while(0);
//#define UNUSED(A) (void)(A)
//#define STRINGIFY(A) #A

//#define ERROR(MSG) fprintf(stderr, BOLD_ON "%s:%d:" COLOR_OFF " %s: "  ERROR_ON "error:" COLOR_OFF " %s: %s\n", __FILE__, __LINE__, __func__, MSG, strerror(errno)); 
//#define MCHECK(PTR) if (PTR == NULL) { ERROR("malloc"); exit(EXIT_FAILURE); }
//#define FOPEN(FILENAME) (FILE*)({ errno = 0; FILE *FP = fopen(FILENAME, "r"); if (FP  == NULL) { ERROR(FILENAME); exit(EXIT_FAILURE); }; FP; })
//#define MANY(TYPE,COUNT) (TYPE*)({ errno = 0; TYPE *RET = calloc(COUNT, sizeof(TYPE)); MCHECK(RET); RET; })
//#define NEW(TYPE) MANY(TYPE,1)


typedef struct _long_array_s {
    size_t length;
    long *p;
} long_array_s;

typedef void (*operation)(long_array_s *array, long *value);
typedef struct _operation_s {
    char *op_name;
    long *op_value;
    operation op;
} operation_s;

typedef struct _op_array_s {
    size_t length;
    operation_s *p;
} op_array_s;



void parse_iput(char *filename, op_array_s *operations);

size_t compute(op_array_s*operations, size_t cards_number, size_t times, long result);

void deal_into_new_stack(long_array_s *array, long *value);
void deal_with_increment(long_array_s *array, long *value);
void cut                (long_array_s *array, long *value);


#endif