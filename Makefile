CC = gcc
CFLAGS = -Wall -Wextra -Werror -O0 -g

all: directories aoc aoc_macro

directories:
	@mkdir -p bin
	@mkdir -p obj

aoc: obj/main.o obj/aoc.o
	@echo "Making executable: "$@
	@$(CC) $^ -o bin/$@ -lm

aoc_macro: src/main_macro.c
	@echo "Making executable: "$@
	@$(CC) $^ -o bin/$@ -lm

obj/%.o: %.c
	@$(CC) $(CFLAGS) -I lib -o $@ -c $<

clean:
	@rm -rvf bin obj
	@ipcrm -a
	@echo "Removed object files and executables..."

.PHONY: all clean