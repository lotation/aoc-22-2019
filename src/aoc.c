#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <assert.h>

#include "aoc.h"


extern const operation_s ops_map[OP_COUNT];


void parse_iput(char *filename, op_array_s *operations)
{   
    if (operations == NULL) {
        return;
    }

    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        perror("fopen failed  ");
        exit(EXIT_FAILURE);
    }


    int c = 0;
    char name_buffer[MAX_LINE];
    char val_buffer[MAX_OP_DIGITS];
    size_t name_indx = 0, val_indx = 0, op_indx = 0;
    long val = 0;


    while ((c = getc(fp)) != EOF) {
        if ((c >= 'a' && c <= 'z') || c == ' ') {
            name_buffer[name_indx++] = c;
        } else if ((c >= '0' && c <= '9') || c == '-') {
            name_buffer[name_indx] = '\0';
            val_buffer[val_indx++] = c;
        } else if (c == '\n') {
            val_buffer[val_indx] = '\0';

            val = strtol(val_buffer, NULL, BASE);

            for (size_t k = 0; k < OP_COUNT; k++) {
                assert(ops_map[k].op_name != NULL);
                assert(ops_map[k].op != NULL);

                if (strstr(name_buffer, ops_map[k].op_name) != NULL) {
                    // check for space
                    if (op_indx + 1 > operations->length) {
                        operations->length  += 1;
                        operation_s *tmp = (operation_s *) reallocarray(operations->p, operations->length, sizeof(operation_s));
                        if (tmp == NULL) {
                            perror("malloc failed  ");
                            exit(EXIT_FAILURE);
                        }
                        operations->p = tmp;
                    }

                    // copy name
                    operations->p[op_indx].op_name = (char *) calloc(strlen(name_buffer) + 1, sizeof(char));
                    if (operations->p[op_indx].op_name == NULL) {
                        perror("malloc failed  ");
                        exit(EXIT_FAILURE);
                    }
                    memcpy(operations->p[op_indx].op_name, name_buffer, strlen(name_buffer) + 1);

                    if (strstr(operations->p[op_indx].op_name, "stack") != NULL) {
                        operations->p[op_indx].op_value = NULL;
                    } else {
                        // copy value
                        operations->p[op_indx].op_value = (long *) calloc(1, sizeof(long));
                        if (operations->p[op_indx].op_value == NULL) {
                            perror("malloc failed  ");
                            exit(EXIT_FAILURE);
                        }
                        memcpy(operations->p[op_indx].op_value, &val, sizeof(long));
                    }

                    // set operation
                    operations->p[op_indx].op = ops_map[k].op;

                    op_indx += 1;
                }
            }

            val_indx = name_indx = 0;
        }
    }    
    
    fclose(fp);
}

size_t compute(op_array_s*operations, size_t cards_number, size_t times, long to_match)
{
    long_array_s cards;
    cards.length = cards_number;
    cards.p = (long *) calloc(cards.length, sizeof(long));
    if (cards.p == NULL) {
        perror("malloc failed  ");
        exit(EXIT_FAILURE);
    }

    for (size_t i = 0; i < cards.length; i++) {
        cards.p[i] = i;
    }

    for (size_t i = 0; i < times; i++)
    {
        for (size_t op_indx = 0; op_indx < operations->length; op_indx++) {
#ifdef _DEBUG
            // accessing operations->p[op_indx].op_value[0]) when deal into new stack causes a segfault because of NULL
            printf("%s", operations->p[op_indx].op_name);
            if (strstr(operations->p[op_indx].op_name, "stack") != NULL) {
                printf("\n");
            } else {
                printf("%ld\n", operations->p[op_indx].op_value[0]);
            }
#endif
            operations->p[op_indx].op(&cards, operations->p[op_indx].op_value);
        }
    }
    
    size_t result = -1;
    
    for (size_t i = 0; i < cards.length; i++) {
        if (cards.p[i] == to_match) {
            result = i;
            goto cleanup;
        }  
    }

cleanup:
    free(cards.p);
    return result;
}

void deal_into_new_stack(long_array_s *array, long *value)
{
    if (value != NULL) {
        fprintf(stderr, "%ld should be set to NULL in 'deal into new stack'!\n", *value);
        exit(EXIT_FAILURE);
    }

    for (size_t i = 0; i < ceil((double)array->length / 2); i++) {
        long tmp = array->p[i];
        array->p[i] = array->p[array->length - i - 1];
        array->p[array->length - i - 1] = tmp;
    }
}

void deal_with_increment(long_array_s *array, long *value)
{
    if (value == NULL) {
        fprintf(stderr, "value shouldn't be NULL in 'deal with increment'!\n");
        exit(EXIT_FAILURE);
    }

    if (*value == 0) {
        return;
    }

    size_t arr_indx = 0, new_indx = 0;
    long_array_s *new = (long_array_s *) malloc(sizeof(long_array_s));
    memset(new, 0x0, sizeof(long_array_s));
    new->length = array->length;
    new->p = (long *) calloc(new->length, sizeof(long));
    if (new->p == NULL) {
        perror("malloc failed  ");
        exit(EXIT_FAILURE);
    }

    while (new_indx < array->length) {
        new->p[arr_indx % array->length] = array->p[new_indx];
        arr_indx += *value;
        new_indx++;
    }

    long_array_s tmp = *array;
    *array = *new;
    *new = tmp;

    free(new->p);
    free(new);
}

void cut(long_array_s *array, long *value)
{
    if (value == NULL) {
        fprintf(stderr, "value shouldn't be NULL in 'cut'!\n");
        exit(EXIT_FAILURE);
    }

    if (*value < 0) {
        *value = array->length + *value;
    }

    long *tmp = (long *) calloc(*value, sizeof(long));
    if (tmp == NULL) {
        perror("malloc failed  ");
        exit(EXIT_FAILURE);
    }

    for (size_t i = 0; i < (size_t)(*value); i++) {
        tmp[i] = array->p[i];
    }

    for (size_t i = 0; i < array->length - *value; i++) {
        array->p[i] = array->p[i + *value];
    }
    
    for (size_t i = 0; i < (size_t)(*value); i++) {
        array->p[array->length - i - 1] = tmp[*value - i - 1];
    }

    free(tmp);
}
