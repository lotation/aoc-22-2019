#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <assert.h>

#include "aoc.h"


const operation_s ops_map[OP_COUNT] = {
    { "deal into new stack", NULL, deal_into_new_stack },
    { "deal with increment", NULL, deal_with_increment },
    { "cut",                 NULL, cut                 },
};


int main(int argc, char *argv[]) 
{    
    errno = 0;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s INPUT_FILE\n\n", argv[0]);
        exit(EXIT_FAILURE);
    }    


    op_array_s *operazioni = (op_array_s *) calloc(1, sizeof(op_array_s));
    if (operazioni == NULL) {
        perror("malloc failed  ");
        exit(EXIT_FAILURE);
    }
    operazioni->length = 1;
    operazioni->p = (operation_s *) calloc(operazioni->length, sizeof(operation_s));
    if (operazioni->p == NULL) {
        perror("malloc failed  ");
        exit(EXIT_FAILURE);
    }


    parse_iput(argv[1], operazioni);


    size_t res = compute(operazioni, 10007, 1, 2019);
    printf("cards[%zu] => %ld\n", res, (long)2019);




    /* cleanup */
    for (size_t i = 0; i < operazioni->length; i++) {
        free(operazioni->p[i].op_name);
        free(operazioni->p[i].op_value);
    }
    free(operazioni->p);
    free(operazioni);


    return 0;
}
