#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <math.h>
#include <assert.h>

#include "aoc_macro.h"

typedef long long llong;

GEN_AOC(long, l);
//GEN_AOC(llong, ll);

int main(int argc, char *argv[]) 
{    
    errno = 0;

    if (argc != 2) {
        fprintf(stderr, "Usage: %s INPUT_FILE\n\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    CALC_AOC(long, ld, 10007, 1, 2019);

    //CALC_AOC(llong, lld, 119315717514047, 101741582076661, 2020);


    return 0;
}
